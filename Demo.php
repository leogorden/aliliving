<?php
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

namespace aliliving;

function example()
{
    $path = '/cloud/thing/properties/get';
    $host = 'https://api.link.aliyun.com';
    $appKey = '';
    $appSecret = '';

    $request = new HttpRequest($host, $path, HttpMethod::POST, $appKey, $appSecret);
    //设置API版本和参数，其中，res为授权的资源ID。grantType为project时，res的值为project的ID。
    $body = '{"version":"1.0","request":{"apiVer":"1.0.0"},"params":{"productKey":"a1kvop3jTHg","deviceName":"e8db8486a5b1","iotId":""}}';

    //设定Content-Type
    $request->setHeader(HttpHeader::HTTP_HEADER_CONTENT_TYPE,
                        ContentType::CONTENT_TYPE_JSON);

    //设定Accept
    $request->setHeader(HttpHeader::HTTP_HEADER_ACCEPT,
                        ContentType::CONTENT_TYPE_JSON);

    if (strlen($body) > 0) {
        $request->setHeader(HttpHeader::HTTP_HEADER_CONTENT_MD5,
                            base64_encode(md5($body, true)));
        $request->setBodyString($body);
    }

    //指定参与签名的header
    $request->setSignHeader(SystemHeader::X_CA_TIMESTAMP);

    $response = HttpClient::execute($request);
    echo $response->getBody();
}
example();
